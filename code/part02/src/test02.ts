enum Gender {
    Male,
    Female
}

let human: {name: string, size?: number, sex: Gender};
human = {
    name: "iFinder",
    size: 18,
    sex: Gender.Male
};

console.log(human.name);