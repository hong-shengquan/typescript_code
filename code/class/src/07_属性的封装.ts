(()=>{
    // 定义一个人类
    class Person {
        /**
         * 有的时候我们不希望对象中的属性被随意的修改,这时就需要将属性封装起来
         * 这时候可以通过属性修饰符来对属性进行封装
         * 
         * public: (默认)修饰的属性可以再任意位置被访问
         * private: 私有属性,只能在类的内部进行访问与修改
         *   --这时通过在类中添加方法,让外部通过这个方法访问这个属性
         *   --这时修改与访问的方法都是开发者提供的,这样这个属性的访问就变得可控了
         * protected: 当前类和子类能都访问与修改,外部不能访问与修改
         */
        private _name:string
        private _age:number

        constructor(name:string, age:number) {
            this._name = name
            this._age = age
        }

        // 给外部的类提供一个访问私有属性的方法
        getName():string {
            return this._name;
        }
        // set的时候限制name的类型
        setName(name:string) {
            this._name = name;
        }
        // set的时候限制年龄不会为负值
        setAge(age:number) {
            if(age >= 1) {
                this._age = age;
            }
        }

        // TS中独有的getter和setter方法
        get age():number {
            return this._age >= 1 ? this._age : 1;
        }
        set age(age:number) {
            if(age >= 1) {
                this._age = age;
            }
        }
    }

    // TS独有的写法可以直接用类似属性的方法来进行设置
    const p = new Person("iFinder", 25);
    console.log(p.age);  // 这时会调用get age方法获取属性
    p.age = -1;  // 这时会调用set age方法设置属性
    console.log(p.age);  // 方法中有判断,此时的打印依旧是25
})()