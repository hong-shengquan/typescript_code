(()=>{
    // type关键字描述对象的类型
    type myType = {
        name: string,
        age: number
    }

    /**
     * 接口用来定义一个类的结构
     * 可以定义一个类型中应该包含哪些方法
     */
    interface myInterFace {
        name: string
        age: number
    }

    // 接口可以重复声明,但是在作为类型使用的时候属性是叠加的
    interface myInterFace {
        gender: string
    }

    /**
    * 接口可以在定义类的时候限制类的结构
    * 接口中的所有属性都不能有实际的值
    * 接口值定义对象的结构,不考虑实际的值
    */
    interface inter{
        name: string
        sayHello(): void
    }

    // 定义类的时候可以让这个类实现这个接口
    class MyClass implements inter {
        name: string = "iFinder"

        sayHello(): void {
            //...
        }
    }
})()