(()=>{
    /**
     * 定义函数或者类的时候,如果遇到类型不明确的,可以使用泛型
     *   <V>定义了一个叫V的泛型
     */
    function fn<V>(a: V): V {
        return a;
    }

    // 直接调用具有泛型的函数,这时泛型的类型就是number
    let a:number = fn(10)
    // 可以直接指定泛型的类型
    let s:string = fn<string>('iFinder')

    //可以指定多个泛型
    function fn1<K, T>(a: T, b: K): T {
        return a;
    }

    // 定义的时候就指定泛型的类型
    interface Inter{
        length:number
        getLength(): number
    }
    abstract class Animal {
        info:string;
        constructor(private _name:string, private _age:number) {
            this.info = `name: ${_name}, age: ${_age}`
        }
        get name(){
            return this._name
        }
        set age(age:number) {
            this._age = age
        }
    }

    function fn2<T extends Inter, K extends Animal>(a: T, b: K): K {
        return b
    }

    // 通过泛型指定类中的某种类型
    class MyClass<T>{
        constructor(public name:T) {

        }
    }
    // 创建类的时候确定泛型类型
    let my = new MyClass<string>("iFinder")
})()