(function(){
    // 创建一个表示动物的类
    // 通过继承,可以把多个类共有得属性与方法抽象成公共得父类,这样就提高了代码得复用性
    class Animal {
        name: string 
        age: number
        constructor(name: string, age: number){
            this.name = name
            this.age = age
        }
        sayHello() {
            console.log(`${this.name} 在 叫 !`)
        }
    }

    // 让Dog类继承自动物类
    // 这时Animal就是Dog的父类,子类将会拥有父类所有的方法与属性
    class Dog extends Animal {
        // 可以实现自己独有得方法
        run() {
            console.log(`${this.name} 在 跑!`)
        }
        // 可以重写父类的方法实现自己特有的功能
        // 子类的方法会覆盖父类的方法
        sayHello() {
            console.log(`${this.name} 在 汪汪汪 !`)
        }
    }

    class Cat extends Animal {
        sayHello() {
            console.log(`${this.name} 在 喵喵喵 !`)
        }
    }

    const dog = new Dog('小黑狗', 4)
    const cat = new Cat('小花猫', 6)
    dog.sayHello()
    cat.sayHello()
})()