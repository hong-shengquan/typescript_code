"use strict";
(() => {
    // 定义一个人类
    class Person {
        constructor(name, age) {
            this._name = name;
            this._age = age;
        }
        // 给外部的类提供一个访问私有属性的方法
        getName() {
            return this._name;
        }
        // set的时候限制name的类型
        setName(name) {
            this._name = name;
        }
        // set的时候限制年龄不会为负值
        setAge(age) {
            if (age >= 1) {
                this._age = age;
            }
        }
        // TS中独有的getter和setter方法
        get age() {
            return this._age >= 1 ? this._age : 1;
        }
        set age(age) {
            if (age >= 1) {
                this._age = age;
            }
        }
    }
    // TS独有的写法可以直接用类似属性的方法来进行设置
    const p = new Person("iFinder", 25);
    console.log(p.age); // 这时会调用get age方法获取属性
    p.age = -1; // 这时会调用set age方法设置属性
    console.log(p.age); // 方法中有判断,此时的打印依旧是25
})();
