"use strict";
(() => {
    /**
     * 定义函数或者类的时候,如果遇到类型不明确的,可以使用泛型
     *   <V>定义了一个叫V的泛型
     */
    function fn(a) {
        return a;
    }
    // 直接调用具有泛型的函数,这时泛型的类型就是number
    let a = fn(10);
    // 可以直接指定泛型的类型
    let s = fn('iFinder');
    //可以指定多个泛型
    function fn1(a, b) {
        return a;
    }
    class Animal {
        constructor(_name, _age) {
            this._name = _name;
            this._age = _age;
            this.info = `name: ${_name}, age: ${_age}`;
        }
        get name() {
            return this._name;
        }
        set age(age) {
            this._age = age;
        }
    }
    function fn2(a, b) {
        return b;
    }
    // 通过泛型指定类中的某种类型
    class MyClass {
        constructor(name) {
            this.name = name;
        }
    }
    // 创建类的时候确定泛型类型
    let my = new MyClass("iFinder");
})();
