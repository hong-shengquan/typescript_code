"use strict";
(() => {
    // 定义类的时候可以让这个类实现这个接口
    class MyClass {
        constructor() {
            this.name = "iFinder";
        }
        sayHello() {
            //...
        }
    }
})();
