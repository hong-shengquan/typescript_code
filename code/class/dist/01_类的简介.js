"use strict";
// 使用class关键字定义一个类
/**
 * 类主要包含两部分:
 * 1.属性
 * 2.方法
 */
class Person {
    constructor() {
        // 示例属性,从属于对象  需要通过类的实例对象去访问
        this.name = "iFinder";
        this.age = 24;
        // 只读属性, readonly开头,不能更改只能读取
        this.country = "china";
    }
    // 方法
    // 同理如果方法用static修饰,该方法便可以通过类去调用
    sayGood() {
        console.log("Good !");
    }
}
// 静态属性,从属于类  可以直接通过类来访问
Person.id = 9527;
const per = new Person();
per.sayGood();
console.log(per.name);
console.log(per.age);
// 静态属性
console.log(Person.id);
