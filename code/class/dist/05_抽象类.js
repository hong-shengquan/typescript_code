"use strict";
(() => {
    /**
     * 以abstract开头的类是抽象类
     * 只能被继承,不能直接实例化
     *
     * 抽象类中可以添加抽象方法(规定一个必须实现的方法,让不同的类去实现)
     */
    class Animal {
        constructor(name) {
            this.name = name;
        }
    }
    class Dog extends Animal {
        // 抽象方法会强制子类去添加具体的实现
        sayHello() {
            console.log(`${this.name} 在 汪汪汪 !`);
        }
    }
    const dog = new Dog('小黑');
    dog.sayHello();
})();
