"use strict";
class Dog {
    /**
     * 构造器,在创建多个对象的时候,创建对象时调用的函数
     */
    constructor(name, age) {
        // this表示的就是当前的示例,可以通过他向新建的对象中添加属性
        this.name = name;
        this.age = age;
    }
    bark() {
        alert('汪 !');
    }
}
class Cat {
    // ts也可以将属性快速的定义在构造器当中, 简化了属性的写法
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}
const dog1 = new Dog('dog1', 4);
const dog2 = new Dog('dog2', 5);
const dog3 = new Dog('dog3', 6);
console.log(dog1);
console.log(dog2);
console.log(dog3);
