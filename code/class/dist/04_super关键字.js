"use strict";
(() => {
    class Animal {
        constructor(name) {
            this.name = name;
        }
        sayHello() {
            console.log("动物叫");
        }
    }
    class Dog extends Animal {
        // 如果在子类中重新实现了构造器,那么需要通过super调用父类的构造器
        constructor(name, age) {
            super(name);
            // 子类新添加的属性需要自己实现
            this.age = age;
        }
        sayHello() {
            // 在类的方法中,super表示当前类的父类
            // 这里可以理解为直接调用了父类的sayHello方法
            super.sayHello();
        }
    }
    const dog = new Dog('小黑', 8);
    dog.sayHello();
})();
