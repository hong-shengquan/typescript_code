/**object类型-表示一个JS对象 */
let oc:object;  //但是这种写法不实用
//对象也可以这么表示
oc = {};
oc = function() {};
//开发的时候我们使用对象其实是更倾向于使用该对象的属性的,可以直接这么写
//表示oc2这个变量指向了一个 内部有name属性的对象
let oc2: {name: string};
oc2 = {name: "iFinder"};
//{}用于指定对象中可以包含那些属性 {属性名1 : 属性类型1, 属性名2: 属性类型2, ...}
//属性名之后加上 ? 表示可选属性,赋值对象时可以缺失
let human: {name: string, age?: number};
human = {name: "iFinder", age: 24};
human = {name: "Nancy"};  //可选属性是可以缺失的
//[变量名 : string] : unknown可以指定任意数量的属性
let oc3: {name: string, [propName: string]: unknown};
oc3 = {name:"Cat", weight: "13kg", age: 5};

//与之类似的可以指定函数的入参与返回值的结构
//表示限制fun为有两个入参类型为number并且返回值类型为number的函数
let fun: (num1: number, num2: number)=> number;
fun = function(a, b): number {
    return a + b;
}

/**============================================================ */

/**array类型 */
//字符串数组
let strArr: string[];
let strArr1: Array<string>;
strArr1 = ['a', 'b', 'c'];
//number数组
let numArr: number[];
let numStr1: Array<number>;
numArr = [1, 2, 3];

/**============================================================ */
/**tupel 元组:固定长度的数组 */
let strTup: [string, number];
strTup = ['one', 123];  //长度固定,类型固定


/**============================================================ */
/**enum 枚举类型 */
enum Gender {
    Male,
    Female
}

let humanBean : {name: string, gender: Gender};
humanBean = {
    name: 'iFinder',
    gender: Gender.Male
}
console.log(humanBean.gender === Gender.Male);

/**============================================================ */
/**补充: 可以通过& 合并对象的属性*/
//表示需要同时满足两个变量的属性
let mix: {name: string} & {color: number}; 
mix = {
    name: "mix12",
    color: 126
}

/**类型的别名 */
//通过type起名,方便同一类型的复用
type myType = string | number | boolean;
let test1: myType;
let test2: myType;