/**字面量 与 联合类型 */
//可以直接使用字面量进行类型声明
let a : 10;
a = 10;
// a = 11;  会报错,用字面量声明后值不可修改

//可以用 | 来限制字面量的范围
//这样该变量只能是这几个字面量中的任意一个
let student : "iFinder" | "Nancy" | "Bob";

//甚至可以通过 | 来为同一个变量指定多个类型
let age : number | string;
age = 24;
age = "35";
//上边的多个类型的用法叫做-->联合类型

/**==================================================== */

/**any类型与unknown类型 */
let every1 : any;
every1 = 123;
every1 = "iFinder";
every1 = true;
//注意:声明变量时不指定类型的话,TS会默认类型为any
let test; //这里声明的结果就是any类型--隐式any
//一个变量设置any后,相当于对它关闭了TS变量的类型检测

//类型安全的any类unknown
let every2: unknown;
every2 = "iFinder";
//any与unknow的区别:
//any类型可以赋值给其它的变量,而unknown类型的值是不能赋值给其它变量的
let str:string;
str = every1; // 不报错TS允许
//str = every2;报错,unknown不能赋值给其它变量

//解决办法1:类型检查
if (typeof every2 === "string") {
    str = every2;
}
//解决办法2:类型断言,告诉编译器变量的实际类型
str = every2 as string;
str = <string>every2;

/**==================================================== */

/**void 与 never -- 设置无返回值的函数 */
//不指定返回值类型的时候TS会进行动态的判断
function fun1(num) {  //此时的返回值为复合类型 boolean | string
    if (num > 0) {
        return true;
    } else {
        return "error";
    }
    
}

//void 表示空,没有返回值的函数
function fun2() : void {
    // return;
    // return undefined;
    //或者没有return都可以
}
//never 表示连空都没有,一般用在异常中断
function error1(errorCode) : never {
    throw new Error("error code" + errorCode);
}