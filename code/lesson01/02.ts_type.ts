//声明了一个类型为number的变量num
let num: number;
//声明类型之后，变量的类型就不能改变<->区别于JavaScript
// num = "123"; 是会报错的
num = 123;
num = 12;

//声明一个字符串类型,可以直接进行赋值
let str : string = "iFinder";

//声明时还有简化的写法,如果声明与赋值同时进行,TS可以自动对变量进行类型检测
let bool = false;
//之后用这个变量就是boolean类型
// bool = 123;  会报错
bool = true;

//JS中的函数是不考虑类型,个数的
function sum(a, b) {
    return a + b;
}
//输出结果:24
console.log(sum(num, num));
//输出结果1212
console.log(sum(num, "12"));

//相比之下TS的函数类型,可以指定入参的类型
function sumTs(a : number, b : number) {
    return a + b;
}
//这样的话在参数传入的时候就会做相应的检查,进而避免类型混乱而造成的错误
console.log(sumTs(123, 123));

//同样,函数的返回值类型也是能够被定义的
function appendStr(s1 : string, s2 : string) : string {
    return s1 + s2;
}